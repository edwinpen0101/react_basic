import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Helloduniacoy from './component/HelloComponent';
import Helloduniacontainer from './container/Helloduniacontainer';

//ini dengan es6
// const Hellodunia = () => {
//     return <p>Hallo dunia coy</p>
// }

//dengan function component
// function Hellodunia(){
//     return <p>ini dengan stateless component</p>
// }

// class Statefullcomponent extends React.Component{
//     render(){
//         return <h1>INi dengan statefull component</h1>
//     }
// }

ReactDOM.render(<Helloduniacontainer />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
